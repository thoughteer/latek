# LaTeX Template Kit (LaTeK)

Organized collection of easy-to-use [LaTeX][latex] templates.

---

## Basic Workflow

**1**. Create an empty [Git][git] repository for your project:
```
#!console
$ mkdir my-awesome-project
$ cd my-awesome-project
$ git init
```

**2**. Create a `README.md` file with a short description of your project:
```
#!console
$ echo 'My awesome project.' > README.md
$ git add .
$ git commit -m 'Add a description'
```

**3**. Add *LaTeK* to the list of remote repositories and fetch the templates:
```
#!console
$ git remote add latek http://bitbucket.org/thoughteer/latek.git
$ git fetch latek
```

**4**. Take a look at the list of available templates:
```
#!console
$ git branch --list -r latek/*
```

**5**. Try out a particular template:
```
#!console
$ git checkout latek/interesting-template
```
Don't forget to revert the changes you've made and switch back to the
`master` branch when you're done:
```
#!console
$ git reset --hard HEAD
$ git clean -f
$ git checkout master
```

**6**. Select the most suitable template and merge it into the `master` branch:
```
#!console
$ git merge latek/selected-template
```

**7**. Start making changes. Read comments for details.

**8**. Compile the document using the [GNU Make][make] utility:
```
#!console
$ make
```

**9**. Check the result, commit the changes, and go back to step **7**.

---

## License

You have the right to do whatever you want to with this project.
I'll be very thankful if you refer it, though.

[git]: http://www.git-scm.com/ "Official site of the Git project"
[latex]: http://www.latex-project.org/ "Official site of the LaTeX project"
[make]: http://www.gnu.org/software/make/ "Official site of the GNU Make project"
